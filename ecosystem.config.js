module.exports = {
  apps: [
    {
      name: "weyoo_back_dev",
      script: "./server.js",
      watch: true,
      env: {
        "NODE_ENV": "development"
      }
    },
    {
      name: "weyoo_back_prod",
      script: "./server.js",
      watch: true,
      env: {
        "NODE_ENV": "production"
      }
    }  
    ]
}