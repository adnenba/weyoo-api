const mongoose = require('mongoose');

const fileSchema = new mongoose.Schema({
    name: String,
    url: String,
    host: String,
    _user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    _annonce: { type: mongoose.Schema.Types.ObjectId, ref: 'Annonce'},    
});

const File = mongoose.model('File', fileSchema);

module.exports = {
    File
}