const mongoose = require('mongoose');

const screenControl = new mongoose.Schema({
    label: String,
    possibleValues: [{
        categoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
        label: String
    }],
    isRequired: {type: Boolean},
});

const ScreenControl = mongoose.model('ScreenControl', screenControl);

const attributeSchema = new mongoose.Schema({
    categoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
    screenControlId: {type: mongoose.Schema.Types.ObjectId, ref: 'ScreenControl'}
});

const Attribute = mongoose.model('Attribute', attributeSchema);

module.exports.ScreenControl = ScreenControl;
module.exports.Attribute = Attribute;