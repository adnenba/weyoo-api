const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const config = require('config');
const jwt = require('jsonwebtoken');
const { htmlEmailVerif, htmlPassReset, subjectEmailVerif,
				subjectPassReset } = require('../constants/constants');
const sendMail = require('../mail/mailer');
const addressSchema = new mongoose.Schema({
    region : {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Region'
    },
    streetName: {
        type: String,
        maxlength: 50,
    },
    postalCode: {
        type: String,
    }
});
//Region Schema
const regionSchema = new mongoose.Schema({
	name: String
});

const Region = mongoose.model('Region', regionSchema);

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
        required: true,
        minlength: 2,
        maxlength: 50
	},
	lastName: {
		type:String,
        required: true,
        minlength: 2,
        maxlength: 50
	},
	birthday: {
		type: Date
	},
	email : {
		type: String,
		lowercase: true,
		required: true,
		unique: true
	},
	password: {
		type: String,
		minlength: 6,
		maxlength: 255,
	},
	phoneNumber : {
		type: String,
		maxLength: 8
	},
	file: {type: mongoose.Schema.Types.ObjectId, ref: 'File'},
	annonces: [
	{
		type: mongoose.Schema.Types.ObjectId,
		 ref: 'Annonce'
	}],
	facebookId: String,
	googleId: String,
	address: {type: addressSchema},
	emailVerified: {
		type: Boolean,
		default: false
	},
	emailVerificationToken: {
		type: String
	},
	passwordResetToken : {
		type: String
	},
	isActive: {
		type: Boolean,
		default : false
	},
	phoneVerificationCode :{
		type: String
	},
	phoneVerified: {
		type: Boolean,
		default: false
	},
	role: {
		type: String,
	}
});
userSchema.index({firstName: 'text', lastName: 'text'});

function validateUser(user) {
    const schema = {
        firstName: Joi.string().min(2).max(50).required(),
        lastName: Joi.string().min(2).max(50).required(),
        birthday: Joi.date().required(),
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(6).max(255).required(),
		phoneNumber: Joi.string().allow(''),
		file: Joi.string(),
		address: {
			region: Joi.objectId().required(),
       	 	streetName: Joi.string().min(5).max(50).allow(''),
       		postalCode: Joi.string().length(4).allow(''),
		}
    }
    return  Joi.validate(user, schema);
};

userSchema.methods.generateToken = function() {
	const token = jwt.sign({_id: this._id}, config.get('jwtPrivateKey'));
    return token;
};
userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({_id: this._id, role: this.role}, config.get('jwtPrivateKey')
									, {expiresIn: '3d'});
    return token;
};
userSchema.methods.sendEmailVerification = function (user) {
	sendMail(user,subjectEmailVerif, htmlEmailVerif);
}
userSchema.methods.sendPasswordReset = function (user) {
	sendMail(user, subjectPassReset, htmlPassReset );
}
const User = mongoose.model('User', userSchema);

module.exports.User = User;
module.exports.validateUser = validateUser;
module.exports.Region = Region;
