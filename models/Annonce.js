const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const sendMail = require('../mail/annonceMail');
const { htmlAnnonceMail, subjectAnnonceMail } = require('../constants/constants');
const annonceSchema = new mongoose.Schema({
  title: String,
  description: String,
  _user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  files: [{
    file: {type: mongoose.Schema.Types.ObjectId, ref: 'File'},
  }],
  video: String,
  rank: {
    type: Number,
    default: 0
  },
  isActive: {
    type: Boolean,
    default: false
  },
  region: {
    type: mongoose.Schema.Types.ObjectId, ref: 'Region'
  },
  phoneNumber : String,
  dateCreated: {
    type: Date,
    default: Date.now
  },
  typeAnnonce : {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
  category: {
    type: mongoose.Schema.Types.ObjectId, ref: 'Category'
  },
  price: {
    type: Number
  },
  // Vehicule
  makes: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
  model: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
  year: Number,
  fiscalPower: Number,
  fuel: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
  color: String,
  transaction: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'}, 
  // Immobilier
  transactionType: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
  area : Number,
  roomNumber: Number,
  // Informatique
  deviceCondition: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'}
});

annonceSchema.index({title: 'text', description: 'text'});

function validateAnnonce(annonce) {
  const schema = {
      title: Joi.string().min(5).max(50).required(),
      description: Joi.string().min(5).max(50).required(),
      category: Joi.objectId().required(),
      region: Joi.objectId().required(),
      price: Joi.number().required(),
      typeAnnonce: Joi.objectId().required(),
      files: Joi.array(),
      makes: Joi.objectId(),
      model: Joi.objectId(),
      year: Joi.number(),
      fiscalPower: Joi.number(),
      color: Joi.string(),
      fuel: Joi.objectId(),
      transaction: Joi.objectId(),
      transactionType: Joi.objectId(),
      area: Joi.number(),
      roomNumber: Joi.number(),
      deviceCondition: Joi.objectId(),
      video: Joi.string()
  }
  return  Joi.validate(annonce, schema);
};

annonceSchema.methods.sendMail = function(annonce) {
  sendMail(annonce,  subjectAnnonceMail,htmlAnnonceMail);
}

const Annonce = mongoose.model('Annonce', annonceSchema);
module.exports.validateAnnonce = validateAnnonce;
module.exports.Annonce = Annonce;
