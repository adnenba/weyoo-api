const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const categorySchema = new mongoose.Schema({
  label: String,
  parentId: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  serviceId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Service'
  },
  screenControlId: { type: mongoose.Schema.Types.ObjectId, ref: 'ScreenControl'}
});
categorySchema.index({label: 'text'});

function validateCategory (category) {
  const schema = {
    parentId: Joi.objectId(),
    serviceId: Joi.objectId(),
    label: Joi.string().max(50).required()
  }
  return  Joi.validate(category, schema);
};

const Category = mongoose.model('Category', categorySchema);

module.exports.Category = Category;
module.exports.validateCategory = validateCategory;

