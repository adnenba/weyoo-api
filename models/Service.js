const mongoose = require('mongoose');
const Joi = require('joi');
const serviceSchema = new mongoose.Schema({
  label : String,
  description: String,
});
const Service = mongoose.model('Service', serviceSchema);

function validateService (service) {
  const schema = {
    label: Joi.string().max(50).required(),
    description: Joi.string().max(50)
  }
  return  Joi.validate(service, schema);
};

module.exports.Service = Service;
module.exports.validateService = validateService;

