const express = require('express');
const app = express();
const config = require('config');

require('./startup/routes')(app);
require('./startup/db')();

const getConfig = (conf) => {
	if(!config.get(conf)) {
		console.log('FATAL ERROR: ' + conf + ' is not defined.');
		process.exit(1);
	};
}
getConfig('jwtPrivateKey');
getConfig('MONGOUSER');
getConfig('MONGOPASS');
getConfig('MONGOIP');
getConfig('MONGOPORT');

const WEYOUPORT = process.env.NODE_ENV === 'production' ? 'WEYOUPRODPORT' : 'WEYOUDEVPORT';
const PORT = config.get(WEYOUPORT);
app.listen(PORT,(err)=> {
	if(!err) {
		console.log(`Server is running on port ${PORT}`);
	}
});
