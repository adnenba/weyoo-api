const express = require('express');
const helmet = require('helmet');
const user = require('../routes/user');
const auth = require('../routes/auth/auth');
const admin = require('../routes/admin');
const annonce = require('../routes/annonce/annonce');
const fileUpload = require('../routes/fileUpload');
const filter = require('../routes/annonce/filter');
const error = require('../middleware/error');
const passport = require('passport');
const cors = require('cors');
require('../routes/auth/passport-facebook');

module.exports = function (app) {
  app.use(cors());
  app.use(express.static('../public'));
  app.use(express.json());
  app.use(express.urlencoded({extended: true}));
  app.use(helmet());
  app.use(passport.initialize());
  app.use('/api/admin', admin);
  app.use('/api/user', user);
  app.use('/api/user/auth', auth);
  app.use('/api/annonce', annonce);
  app.use('/api/filter', filter);
  app.use('/api/file', fileUpload);
  app.use(error);
}
