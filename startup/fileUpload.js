const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, './public/uploads/')
	},
	filename: function (req, file, cb) {
	  cb(null, file.fieldname + '-' + Date.now()+ path.extname(file.originalname));
	}
});
const upload = multer({
storage: storage,
fileFilter: function (req, file, cb) {
			const ext = path.extname(file.originalname);
			if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
					return cb(null,false);
			}
			cb(null, true);
}
});

module.exports.upload = upload;