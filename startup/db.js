const mongoose = require('mongoose');
const config = require('config');

module.exports = function () {
  mongoose.connect('mongodb://' + config.get('MONGOUSER') + ':' + config.get('MONGOPASS') + '@' + config.get('MONGOIP') + ':' + config.get('MONGOPORT') + '/testWereact', { useNewUrlParser: true })
  	.then(() => { console.log('Connected to MongoDB') })
  	.catch(err => { console.log('Error connecting to mongo.') });
}
