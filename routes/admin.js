const express = require('express');
const router = express.Router();
const adminAuth = require('../middleware/adminAuth');
const signup = require('../constants/signup');
const { User, Region } = require('../models/User');
const { Annonce } = require('../models/Annonce');
const { ScreenControl, Attribute} = require('../models/Attribute');

// create admin account. 
router.post('/create_user', adminAuth, async(req,res,next)=> {
  await signup(req,res,next,'user');
});

// GET user with the given ID.
router.get('user/:id', adminAuth,async(req,res,next) => {
    try {
        const user = await User.findById(req.params.id);
        if(user) {
            res.json(user);
        } else {
            res.status(404).json('User not found');
        }
    } catch (error) {
      next(error);
    }
});

// DELETE , delete user with the given ID.
router.delete('user/:id', adminAuth, async(req,res,next) => {
    try {
        const user = await User.findById(req.params.id);
        if(!user) return res.status(404).json('User not found.');
        const file = await File.findById(req.user.file);
        if(file) {
          await file.remove();
        }
        await user.remove();
        res.json('User deleted.');
    } catch (error) {
          next(error);
    }
});

// Create region TODO: Add adminth auth.
router.post('/create-region', async (req,res)=> {
    try {
        const { name } = req.body;
        if(!name) return res.status(400).json('Name is required.');
        const region = new Region({name});
        await region.save();
        res.json(region);
    } catch (error) {
        next(error);
    }
});

// Get all annonces
router.get('/annonce', async(req,res,next)=> {
    try {
        const annonces = await Annonce.find();
        res.json(annonces);
    } catch (error) {
        next(error);
    }
});
// Activate annonce
router.post('/activate_annonce/:id', adminAuth, async(req,res,next)=> {
    try {
        const annonce = await Annonce.findById(req.params.id);
        if(!annonce) return res.status(404).json('Annonce not found.');
        if(!annonce.isActive) return res.status(400).json('Annonce is already active');
        annonce.isActive = true;
        await annonce.save();
        res.json(annonce);       
    } catch (error) {
        next(error);
    }
});

router.post('/create_screen_control', async(req,res,next)=> {
    try {
        const { label, possibleValues } = req.body
        if(!label) return res.status(400).json('Label is required.');
        const screenControl = new ScreenControl({label, possibleValues});
        await screenControl.save();
        res.json(screenControl);
    } catch (error) {
        next(error);
    }
});
router.get('/screen_control', async(req,res,next)=>{
    try {
        const screenControls = await ScreenControl.find();
        res.json(screenControls);
    } catch (error) {
        next(error);
    }
});

router.post('/create_attributes', async (req,res,next)=> {
    try {
        const { categoryId,screenControlId, isRequired } = req.body;
        const attributes = new Attribute({
            categoryId, screenControlId, isRequired
        });
        await attributes.save();
        res.json(attributes);
    } catch (error) {
        next(error);
    }
});

router.get('/attributes/:id', async(req,res,next)=> {
    try {
        const attribute = await Attribute.findOne({categoryId: req.params.id});
        res.json(attribute); 
    } catch (error) {
        next(error);
    }
});

router.put('/update_user/:id', adminAuth, async(req,res,next)=> {
    try {
        const user = await User.findById(req.params.id);
        if(!user) return res.status(404).json('User not found.');
        if(req.body.firstName) user.firstName = req.body.firstName;
        if(req.body.lastName) user.lastName = req.body.lastName;
        if(req.body.phoneNumber) user.phoneNumber = req.body.phoneNumber;
        if(req.body.address) user.address = req.body.address;
        const updatedUser = await user.save();
        res.json(_.pick(updatedUser,['_id', 'firstName', 'lastName',
        'birthday','email', 'phoneNumber', 'address', 'file', 'role']));
    } catch(error) {
        next(error);
    }
});

router.get('/activate_user/:id', adminAuth, async(req,res,next)=>{
    try {
        const user = await User.findById(req.params.id);
        if(!user) return res.status(404).json('User not found.');
        if(user.isActive) return res.status(400).json('User active.');
        user.isActive = true;
        await user.save();
        res.json('User activated');
    } catch (error) {
        next(error);
    }
});
module.exports = router;
