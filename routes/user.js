const _ = require('lodash');
const express = require('express');
const router = express.Router();
const { User, Region } = require('../models/User');
const userAuth = require('../middleware/auth');
const adminAuth = require('../middleware/adminAuth');
const signup = require('../constants/signup');

// GET list of Users.
router.get('/', adminAuth, async(req,res,next)=> {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        next(error);
    }
});

// POST Create user.
router.post('/', async(req,res,next) => {
  await signup(req,res,next,'user');
});

// PUT, update user.
router.put('/update_profile', userAuth, async(req,res,next) => {
    try {
      const user = await User.findById(req.user._id);
      if(!user) return res.status(404).json('User not found.');
      if(req.body.firstName) user.firstName = req.body.firstName;
      if(req.body.lastName) user.lastName = req.body.lastName;
      if(req.body.phoneNumber) user.phoneNumber = req.body.phoneNumber;
      if(req.body.address) user.address = req.body.address;
      const updatedUser = await user.save();
      res.json(_.pick(updatedUser,['_id', 'firstName', 'lastName',
      'birthday','email', 'phoneNumber', 'address', 'file', 'role']));
    } catch(error) {
        next(error);
    }
});
// Deactive account.
router.get('/deactivate', userAuth,  async(req,res,next) => {
  try{
    const user = await User.findById(req.user._id);
    if(!user) return res.status(404).json('User not found.');
    if (!user.isActive) return res.status(400).json('Account already deactivated.');
    user.isActive = false;
    await user.save();
    res.json('Account deactivated.');
  } catch(error) {
    next(error);
  }
});

// GET current user.
router.get('/me', userAuth, async(req,res,next) => {
  try {
    const user = await User.findById(req.user._id).select('-password -emailVerificationToken -passwordResetToken');
    if(!user) return res.status(404).json('User not available');
    res.json(user);
  } catch (error) {
    next(error);
  }
});

router.get('/regions', async(req,res,next)=>{
  try {
    const regions = await Region.find();
    res.json(regions);
  } catch (error) {
    next(error);
  }

});
module.exports = router;
