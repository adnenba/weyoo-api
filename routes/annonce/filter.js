const express = require('express');
const router = express.Router();
const { Annonce  } = require('../../models/Annonce');
const adminAuth = require('../../middleware/adminAuth');
const { User } = require('../../models/User');

router.post('/price', async(req,res,next)=> {
    try {
        const annonce = await Annonce.find({$and: [{ price: { $gte: req.body.from }}, {price: {$lte: req.body.to}}]});
        res.json(annonce);    
    } catch (error) {
        next(error);
    }   
});

router.get('/users/region/:id', adminAuth, async(req,res,next)=> {
    try {
        const users = await User.find({$text: {$search: req.params.query}});
        res.json(users);    
    } catch (error) {
        next(error);
    }
});

router.get('/users/:query', adminAuth, async(req,res,next)=>{
    try {
        const users = await User.find({$and: [{ price: { $gte: req.body.from }}, {price: {$lte: req.body.to}}]});
        res.json(users);  
    } catch (error) {
        next(error);
    }
})
module.exports = router;