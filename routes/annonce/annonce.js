const express = require('express');
const router = express.Router();
const { Category, validateCategory } = require('../../models/Category');
const { Service, validateService } = require('../../models/Service');
const auth = require('../../middleware/auth');
const { User } = require('../../models/User');
const { Annonce , validateAnnonce } = require('../../models/Annonce');

const redisClient = require('redis').createClient;
const redis = redisClient(6379, '5.135.164.72');

// Get Annonce with word
router.get('/:query', async(req,res)=> {
  redis.hget('querys', req.params.query, (err,result)=> {
    if(err) next(err);
    if(result) res.json(result);
    else {
      Category.find({$text: {$search: req.params.query}},(err,categories)=>{
        if(err) next(err);
        Annonce.find(
          {$text: {$search: req.params.query}},(err,annonces)=> {
            if(err) next(err);
            redis.hset('querys', req.params.query, JSON.stringify(annonces), (err,result)=> {
              if(err) next(err);
              res.json(annonces);
            });
        });
      });
    }
  });
});
// Category -------------------------
// create category
router.post('/create-category', async(req,res)=> {
  try {
    const { error } = validateCategory(req.body);
    if (error) return res.status(400).json(error.details[0].message);
    let category = new Category({
      label: req.body.label,
      serviceId: req.body.serviceId
    });
    if(req.body.parentId) {
      category.parentId = req.body.parentId;
    }
    category = await category.save();
    res.json(category);
  } catch (error) {
    next(error);
  }
});

// delete category
router.delete('/category/:id', async(req,res, next)=>{
  try {
    const category = await Category.findById(req.params.id);
    if(!category) return res.status(404).json('Category not found.');
    await category.remove();
    res.json('Category removed.');  
  } catch (error) {
    next(error);
  }
});
// edit category
router.put('/edit-category/:id', async(req,res,next)=> {
  try {
    let category = await Category.findById(req.params.id);
    if(!category) return res.status(404).json('Category not found.');
    if(req.body.label) category.label = req.body.label;
    if(req.body.parentId) category.parentId = req.body.parentId;
    if(req.body.serviceId) category.serviceId = req.body.serviceId;
    category = await category.save();
    res.json(category);
  } catch (error) {
    next(error);
  }
});
// Get categories. 
router.get('/category', async(req,res,next)=> {
  try {
    const categories = await Category.find();
    res.json(categories);
    
  } catch (error) {
    next(error);
  }
});

// get category with ID
router.get('/category/:id', async(req,res,next)=> {
  redis.hget('categories', req.params.id,async(err,result)=> {
    if(err) { next(error); }
    if(result) res.json(result);
    else {
      Category.findById(req.params.id,(err,category)=> {
        if(err) return res.status(404).json('Category not found');
        redis.hset('categories', req.params.id, JSON.stringify(category), (err,result)=>{
          if(err) next(error);
          else res.json(category);
        });
      });      
    }
  });
});

router.get('/sub_category/:id', async(req,res,next)=> {
  redis.hget('subcategories', req.params.id, async(err,result)=> {
    if(err) next(err);
    if(result) res.json(result);
    else {
      Category.find({parentId: req.params.id}, (err,result)=> {
        if(err) next(err);
        redis.hset('subcategories', req.params.id, JSON.stringify(result) , (err,categories)=> {
          if(err) next(err);
          res.json(result);
        });
      });
    }
  });
});
// SERVICE -------------------------
// Create Service
router.post('/create-service', async(req,res,next)=>{
  try {
    const { error } = validateService(req.body);
    if (error) return res.status(400).json(error.details[0].message);
    const service = new Service({
      label: req.body.label,
      description: req.body.description
    });
    newService = await service.save();
    res.json(newService);
    
  } catch (error) {
    next(error);
  }
});

// Edit service
router.put('/edit-service/:id', async(req,res)=> {
  try {
    let service = await Service.findById(req.params.id);
    if(!service) return res.status(404).json('Service not found.');
    if(req.body.label) service.label = req.body.label;
    if(req.body.description) service.description = req.body.description;
    service = await service.save();
    res.json(service);
  } catch (error) {
    next(error);
  }
});

// Delete service
router.delete('/service/:id', async(req,res)=>{
  try {
    const service = await Service.findById(req.params.id);
    if(!service) res.status(404).json('Service not found.');
    await service.remove();
    res.json('Service deleted.');
  } catch (error) {
    next(error);
  }
});

// Annonce 
// create annonce.
router.post('/', auth, async(req,res,next)=> {
  const {
    title,
    description,
    price,
    category,
    region,
    typeAnnonce,
    files, video } = req.body; 
  const { error } = validateAnnonce(req.body);
  if(error) return res.status(400).json(error.details[0].message);
  try {
    const user = await User.findById(req.user._id);
    if(!user) return res.status(404).json('User not found.');
    const annonce = new Annonce({
      title,
      description,
      category,
      price,
      region,
      typeAnnonce,
      video
    });
    if(!req.body.phoneNumber) annonce.phoneNumber = user.phoneNumber; 
    if(!user.phoneVerified ) return res.status(400).json('You need to verify your phone.');
    for(let i=0; i < files.length; i++) {
      annonce.files.push(files[i]);
    }
    const foundCategory = await Category.findById(typeAnnonce);
    if(!foundCategory) return res.status(400).json('Category does not match.');
    switch (foundCategory.label) {
      case 'Immobilier':
        annonce.transactionType = req.body.transactionType;
        annonce.area = req.body.area;
        annonce.roomNumber = req.body.roomNumber;
        await annonce.save();
        break;
      case 'Informatique':
        annonce.deviceCondition = req.body.deviceCondition;
        await annonce.save();
      case 'Vehicule': 
        annonce.makes = req.body.makes; 
        annonce.model = req.body.model; 
        annonce.year = req.body.year; 
        annonce.fiscalPower = req.body.fiscalPower;
        annonce.fuel = req.body.fuel;
        annonce.transaction = req.body.transaction;
        await annonce.save();
      default:
        res.status(400).json('Category does not match');
        break;
    }
    annonce.sendMail(annonce);
    res.json(annonce);
  } catch (error) {
    console.log(error);
  }
 
 
});

// update Annonce 

router.put('/edit_annonce/:id', auth, async(req,res,next)=> {
  try {
    const user = await User.findById(req.user._id);
    if(!user) return res.status(404).json('User not found.');
    const annonce = await Annonce.findById(req.params.id);
    if(!annonce) return res.status(404).json('annonce not found.');
    if(annonce._user != req.user._id) return res.status(403).json('unauthorized action.');
    const foundCategory = await Category.findById(annonce.typeAnnonce);
    if(req.body.title) annonce.title = req.body.title;
    if(req.body.description) annonce.description = req.body.description;
    if(req.body.price) annonce.price = req.body.price;
    if(req.body.category) annonce.category = req.body.category;
    if(req.body.region) annonce.region = req.body.region;
    switch (foundCategory.label) {
      case 'Immobilier':
        if(req.body.transactionType) annonce.transactionType = req.body.transactionType;
        if(req.body.area) annonce.area = req.body.area;
        if(req5ba1fdb718c5d81030e99804.body.roomNumber) annonce.roomNumber = req.body.roomNumber;
        await annonce.save();
        break;
      case 'Informatique':
      if(req.body.deviceCondition) annonce.deviceCondition = req.body.deviceCondition;
        await annonce.save();
      case 'Vehicule': 
      if(req.body.makes) annonce.makes = req.body.makes; 
      if(req.body.model) annonce.model = req.body.model; 
      if(req.body.year) annonce.year = req.body.year; 
      if(req.body.fiscalPower) annonce.fiscalPower = req.body.fiscalPower;
      if(req.body.fuel) annonce.fuel = req.body.fuel;
      if(req.body.transaction) annonce.transaction = req.body.transaction;
        await annonce.save();
      default:
        res.status(400).json('Category does not match');
        break;
    }
    res.json(annonce);
  } catch (error) {
    next(error);
  }
}); 

// Delete Annonce 
router.delete('/:id', auth, async(req,res,next)=>{
  try {
    const user = await User.findById(req.user._id);
    if(!user) return res.status(404).json('User not found.');
    const annonce = await Annonce.findById(req.params.id);
    if(!annonce) return res.status(404).json('annonce not found.')
    if(annonce._user != req.user._id) return res.status(403).json('unauthorized action.');
    await annonce.remove();
    res.json('Annonce removed.');
  } catch (error) {
    next(error);
  }
});
// Get Annonces that are active .
router.get('/', async(req,res,next)=> {
  try {
    const annonces = await Annonce.find({isActive: true});
    res.json(annonces);
  } catch (error) {
    next(error);
  }
});

// Get Annonce with ID
router.get('/:id', async(req,res,next)=>{
    redis.hget('annonces', req.params.id,async(err,result)=> {
      if(err) {next(error);}
      if(result) res.json(result);
      else {
        Annonce.findById(req.params.id,(err,annonce)=> {
          if(err) return res.status(404).json('Annonce not found');
          redis.hset('annonces', req.params.id, JSON.stringify(annonce), (err,result)=>{
            if(err) next(error);
            else res.json(annonce);
          });
        });      
      }
    });
});

module.exports = router;
