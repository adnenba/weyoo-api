const passport = require('passport');
const FacebookStrategy = require('passport-facebook');
const {User} = require('../../models/User');

passport.use(new FacebookStrategy({
  clientID: '508164186273874',
  clientSecret: 'e8a213090dd654387b659cdc916edc2b',
  callbackURL: 'http://localhost:3000/api/user/auth/facebook/callback',
  profileFields: ['id', 'displayName','name', 'email']
}, async function(accessToken, refreshToken, profile, done) {
  try {
    const user = await User.findOne({facebookId: profile._json.id});
    if(user) {
      const token = user.generateAuthToken();
      return done(null,token);
    } 
    let newUser = new User({
      facebookId: profile._json.id,
      firstName: profile._json.first_name,
      lastName: profile._json.last_name,
      email: profile._json.email,
      emailVerified: true,
      role: 'user'
    });
    newUser = await newUser.save();
    const token = newUser.generateAuthToken();
    return done(null,token);

  } catch (error) {
    return done(error,null);
  }
}));
