const Joi = require('joi');
const bcrypt = require('bcryptjs');
const express= require('express');
const router = express.Router();
const { User } = require('../../models/User');
const auth = require('../../middleware/auth');
const passport = require('passport');

// LOGIN
router.post('/', async(req,res,next)=>{
  try {
    const { email,password } = req.body;
    // Validate input for errors.
    const { error } = validate(req.body);
    if(error) return res.status(400).json(error.details[0].message);
    const user = await User.findOne({ email });
    if(!user) return res.status(400).json('Invalid email or password.');
    const valid = await bcrypt.compare(password, user.password);
    if(!valid) return res.status(400).json('Invalid email or password.');
    if(!user.emailVerified){
        const verificationToken = user.generateToken();
        user.emailVerificationToken = verificationToken;
        user.sendEmailVerification(user); 
        return res.status(400).json('Verify your email to login.');
    }
    if(!user.isActive) {
      user.isActive = true;
      await user.save();
    }   
    const token = user.generateAuthToken();
    res.json(token);
  } catch(error) {
      res.status(500).json(error.message); 
  }
});

// Verify Email.
router.get('/email_verif/:token', async(req,res,next) => {
    try {
        const user = await User.findOne({emailVerificationToken: req.params.token});
        if(user) {
            if(!user.emailVerified) {
                user.emailVerified = true;
                user.emailVerificationToken = '';
                await user.save();
                res.json('Account verified.');
            } else {
                res.status(400).json('Account already verified. ');
            }
        } else {
            res.status(401).json('Invalid token.');
        }
    } catch (error) {
        next(error);
    }
});

// Resend email.
router.post('/resend_email', auth, async(req,res,next)=> {
    try {
        const user = await User.findById(req.user._id);
        if(!user) return res.status(404).json('User not found.');
        if(user.emailVerified) return res.json('Account already verified. ');
        const verificationToken = user.generateToken();
        user.emailVerificationToken = verificationToken;
        await user.save();
        user.sendEmailVerification(user);
        res.json('email resent to '+user.email);
    } catch (error) {
        next(error);
    }
});

// send email reset link.
router.post('/forgot_password', async(req,res,next)=> {
    const { email } = req.body;
    try {
        const user = await User.findOne({email});
        if(!user) return res.status(404).json('User not found.');
        const token = user.generateToken();
        user.passwordResetToken = token;
        await user.save();
        user.sendPasswordReset(user);
        res.json('Email sent.');

    } catch (error) {
        next(error);
    }
});
// Reset password.
router.post('/reset_password/:token', async(req,res,next)=> {
    try {
        const { password } = req.body;
        const user = await User.findOne({passwordResetToken: req.params.token});
        if(!user) return res.status(401).json('Invalid Token');
        //Hashing the password.
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        user.password = hash;
        user.passwordResetToken='';
        await user.save();
        res.json('Password reset successful.');
    } catch (error) {
          next(error);
    }
});
// Facebook Passport Authentication.
router.get('/facebook', passport.authenticate('facebook', { scope: 'email' }));
router.get('/facebook/callback', async function(req,res,next) {
  await passport.authenticate('facebook',function(err,token){
    if(err) return res.json(err.message);
    res.json(token);
  })(req,res,next);
});

// Verify phone
router.post('/phone_verif',auth, async(req,res) => {
    try {
        const user = await User.findById(req.user._id);
        if(!user) res.status(404).json('User not found.');
        if(user.phoneVerificationCode === req.body.code) {
            user.phoneVerified = true;
            res.json('Phone verified');
        } else {
            res.status(400).json('Wrong verification code.');
        }
    } catch (error) {
        next(error);
    }

});

function validate(user) {
    const schema = {
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().min(6).max(12).required(),
    }
    return Joi.validate(user, schema);
}
module.exports = router;
