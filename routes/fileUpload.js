const express = require('express');
const router = express.Router();
const { upload } = require('../startup/fileUpload');
const auth = require('../middleware/auth');
const { File } = require('../models/File');
const { User } = require('../models/User');
const { Annonce } = require('../models/Annonce');

// Upload profile picture. 
router.post('/upload', auth, upload.single('avatar'), async(req,res,next)=> {
    try {   
        if(req.file) {
            const user = await User.findById(req.user._id);
            if(!user) return res.status(404).json('User not found.');
            const file = new File({
                name: req.file.filename,
            });
            await file.save();
            user.file = file._id;
            await user.save();
            console.log(req.file.originalname);
            res.json('File uploaded');
        } else {
            res.status(400).json('No file was uploaded, only .png .jpg .gif or .jpeg are allowed');
        }
    } catch (error) {
        next(error);
    }
});

// add multipe images to annonce. 
router.post('/annonce_uploads', auth, upload.array('annonce',5), async(req,res,next)=>{
   try {
       const user = await User.findById(req.user._id);
       if(!user) return res.status(404).json('User not found.');
       const files = [];
       if(req.files) {
           for(let i=0; i < req.files.length; i++) {
                let file = new File({
                    name: req.files[i].filename,
                });
                await file.save();
                files.push(file._id);
            }
        res.json(files);
       }
   } catch (error) {
      next(error); 
   }
});

// GET profile image
router.get('/profile_image', auth, async(req,res,next)=> {
    try {
        const user = await User.findById(req.user._id);
        if(!user) return res.status(404).json('User not found.');
        if(!user.file) return res.status(400).json('User did not upload a photo.');
        const file = await File.findById(user.file);
        if(!file) return res.status(404).json('Image not found.');
        res.sendFile(file.name, {'root': 'C:\\Users\\MostfaWindows\\Desktop\\weyou\\public\\uploads\\'});
    } catch (error) {
       next(error); 
    }
});

// put video url. 
router.post('/video/:id', auth,async(req,res,next)=> {
    try {
        const user = await User.findById(req.user._id);
        if(!user) return res.status(404).json('User not found.');
        const annonce = await Annonce.findById(req.params.id);
        if(!annonce) return res.status(404).json('Annonce not found.');  
        const file = new File({
            url: req.body.url,
            host: 'youtube'
        });
        annonce.video.push(file._id);
        await file.save();
        await annonce.save();
        res.json(file);
    } catch(error) {
        next(error);
    }
});

// Delete profile picture of user.
router.delete('/profile_picture', auth,async(req,res,next)=>{
    try {
        const user = await User.findById(req.user._id);
        if(!user) return res.status(404).json('User not found.');
        if(!user.file) return res.status(404).json('No file associated with this user');
        const file =  await File.findById(user.file);
        await file.remove();
        user.file= null;
        await user.save();
        res.json('File deleted.');
    } catch (error) {
        next(error);
    }
});

// delete picture from array. NOT DONE YET.
router.delete('/annonce_picture/:id', auth, async(req,res,next)=> {
    const file  = await File.findById(req.params.id);
    if(!file) return res.status(404).json('No file found.');
    const annonce = await Annonce.findById(req.body.annonce);
    if(!annonce) return res.status(404).json('No annonce associated with this file.');
    Annonce.update({ _id: annonce._id }, { "$pull": { "files": { "_id": file._id } }}, { safe: true, multi:true }, function(err, obj) {
        if(err) return res.status(500).json('Error');
        res.json('File deleted');
    });
    await file.remove();
})
module.exports = router;