const _ = require('lodash');
const bcrypt = require('bcryptjs');
const { User, validateUser } = require('../models/User');

module.exports = async function signup(req, res, next, type) {
  const {
    firstName,
    lastName,
    email,
    password,
    birthday,
    address,
  } = req.body;
  // Validate using JOI
  const { error } = validateUser(req.body);
  if (error) return res.status(400).json(error.details[0].message);
  // Check if the user is already registered.
  try {
    let user = await User.findOne({ email });
    if (user) return res.status(400).json('User already registered.');
    //Hashing the password.
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    user = new User({
      firstName,
      lastName,
      email,
      password: hash,
      birthday,
      address,
      role: type
    });
    if(req.body.phoneNumber) user.phoneNumber = req.body.phoneNumber;
    const verificationToken = user.generateToken();
    user.emailVerificationToken = verificationToken;

    await user.save();
    user.sendEmailVerification(user);
    res.json(
      _.pick(user, [
        '_id',
        'firstName',
        'lastName',
        'birthday',
        'email',
        'phoneNumber',
        'address',
        'file',
        'role'
      ])
    );
  } catch (error) {
    next(error);
  }
};
