const config = require('config');
const SERVER = process.env.NODE_ENV === 'production' ? 'PRODSERVER' : 'DEVSERVER';
const DEV = config.get(SERVER);

const fileTypes = ['image','video', 'pdf', 'doc'];
const subjectEmailVerif = 'Verify your email.';
const subjectPassReset = 'Reset your password.';
const subjectAnnonceMail = 'Une nouvelle annonce a été publiée';
const htmlEmailVerif = (user) => {
    return `Please click this <a href="${DEV}/api/user/auth/email_verif/${user.emailVerificationToken}">link</a> to verify your email`
};
const htmlPassReset = (user) => {
    return `Please click this
    <a href="${DEV}/reset_password/${user.passwordResetToken}">link</a>
    to reset your password.`
};
const htmlAnnonceMail = (annonce) => {
    return `<h1>ID annonce: ${annonce._id}</h1><p>${annonce.title}</p><p>${annonce.description}</p>`;
}
module.exports = {
    fileTypes,
    htmlEmailVerif,
    htmlPassReset,
    htmlAnnonceMail,
    subjectEmailVerif,
    subjectPassReset,
    subjectAnnonceMail,
    
}
