var nodemailer = require('nodemailer');

module.exports = function sendMail (annonce,subject, html) {
    
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'weyoutest@gmail.com',
            pass: 'weyoutest123'
        }
    });
        
    const mailOptions = {
    from: 'weyoutest@gmail.com',
    to: 'weyoutest@gmail.com',
    subject: subject,
    html: html(annonce)
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}